package com.gym.personal.basicrssreader.Home;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gym.personal.basicrssreader.Core.Entities.Item;
import com.gym.personal.basicrssreader.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdapter extends  RecyclerView.Adapter<HomeAdapter.HomeAdapterViewHolder>{

    private List<Item> items;

    HomeAdapter(List<Item> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public HomeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = View.inflate(context, R.layout.item_list_view, null);
        return new HomeAdapterViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapterViewHolder holder, int position) {
        Item item = items.get(position);
        holder.item = item;
        holder.title.setText(item.getTitle());
        holder.subtitle.setText((item.getDescription()));
        loadImage(holder.image, items.get(position).getUrlImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void loadImage(ImageView image, String urlImage) {
        Picasso.get().load(urlImage).into(image);
    }

    static class HomeAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.item_title)
        TextView title;
        @BindView(R.id.item_subtitle)
        TextView subtitle;
        @BindView(R.id.item_image)
        ImageView image;
        private Context context;
        private Item item;
        public HomeAdapterViewHolder(View itemView, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.context = context;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ItemDetail.class);
            intent.putExtra("item", item);
            context.startActivity(intent);
        }
    }
}
