package com.gym.personal.basicrssreader.Home.Contracts;

import com.gym.personal.basicrssreader.Core.Entities.Item;

import java.util.List;

public interface AsyncXmlResponse {

    void onSuccess(List<Item> items);
}
