package com.gym.personal.basicrssreader.Home.Business;

import com.gym.personal.basicrssreader.Core.Contracts.AsyncResponse;
import com.gym.personal.basicrssreader.Core.Contracts.IRssConnector;
import com.gym.personal.basicrssreader.Core.Contracts.IXmlParser;
import com.gym.personal.basicrssreader.Core.Entities.Item;
import com.gym.personal.basicrssreader.Home.Contracts.AsyncXmlResponse;
import com.gym.personal.basicrssreader.Home.Contracts.IHomeDomain;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

public class HomeDomain implements IHomeDomain{

    private IRssConnector connector;
    private IXmlParser xmlParser;

    @Inject
    public HomeDomain(IRssConnector connector, IXmlParser xmlParser) {
        this.connector = connector;
        this.xmlParser = xmlParser;
    }

    @Override
    public void getItems(final AsyncXmlResponse delegate) {
        this.connector.getXmlFromUrl("https://www.xatakandroid.com/tag/feeds/rss2.xml", new AsyncResponse() {
            @Override
            public void onSuccess(InputStream result) {
                try {
                    List<Item> items = xmlParser.parseXml(result);
                    delegate.onSuccess(orderItemsByDate(items));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private List<Item> orderItemsByDate(List<Item> items){
        Collections.sort(items, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                return o1.getPubDate().compareTo(o2.getPubDate());
            }
        });
        return items;
    }
}
