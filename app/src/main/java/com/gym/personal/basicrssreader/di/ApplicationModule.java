package com.gym.personal.basicrssreader.di;

import com.gym.personal.basicrssreader.Core.Contracts.IRssConnector;
import com.gym.personal.basicrssreader.Core.Contracts.IXmlParser;
import com.gym.personal.basicrssreader.Core.Services.RssConnector;
import com.gym.personal.basicrssreader.Core.Services.XmlParser;
import com.gym.personal.basicrssreader.Home.Business.HomeDomain;
import com.gym.personal.basicrssreader.Home.Contracts.IHomeDomain;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Provides
    public IRssConnector providesRssConnector(){
        return new RssConnector();
    }

    @Provides
    public IXmlParser providesXmlParser(){
        return new XmlParser();
    }

    @Provides
    public IHomeDomain providesHomeDomain(IRssConnector rssConnector, IXmlParser xmlParser){
        return new HomeDomain(rssConnector, xmlParser);
    }
}
