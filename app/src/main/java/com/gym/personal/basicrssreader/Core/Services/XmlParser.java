package com.gym.personal.basicrssreader.Core.Services;

import android.text.Html;
import android.util.Xml;

import com.gym.personal.basicrssreader.Core.Contracts.IXmlParser;
import com.gym.personal.basicrssreader.Core.Entities.Item;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class XmlParser implements IXmlParser{
    private String title = null;
    private String description = null;
    private String pubDate = null;
    private boolean isItem = false;
    private List<Item> items = new ArrayList<>();

    @Override
    public List<Item> parseXml(InputStream xml) throws IOException {
        try {
            XmlPullParser xmlPullParser = initializeParser(xml);
            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType = xmlPullParser.getEventType();
                String name = xmlPullParser.getName();

                if(nameIsNull(name)){
                    continue;
                }

                if(typeIsTag(eventType, XmlPullParser.START_TAG)){
                    if(nameIsTag(name, "item")){
                        isItem = true;
                        continue;
                    }
                }
                if(typeIsTag(eventType, XmlPullParser.END_TAG)){
                    if(nameIsTag(name, "item")){
                        isItem = false;
                        title = description = null;
                    }
                    continue;
                }
                String text = getTextFromParser(xmlPullParser);
                setContent(name, text);
                addItem();
            }
        } finally {
            xml.close();
            return items;
        }
    }
    private XmlPullParser initializeParser(InputStream inputStream) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser = Xml.newPullParser();
        xmlPullParser.setInput(inputStream, null);
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.nextTag();
        return xmlPullParser;
    }

    private boolean nameIsNull(String name) {
        return name == null;
    }

    private String getTextFromParser(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        String text = "";
        if (xmlPullParser.next() == XmlPullParser.TEXT) {
            text = xmlPullParser.getText();
            xmlPullParser.nextTag();
        }
        return text;
    }

    private void addItem(){
        if(canAddItem()){
            Item item = new Item(title, getImageUrl(), getHandledDescription(), getDate());
            items.add(item);
        }
    }

    private boolean canAddItem() {
        if (title != null && description != null && pubDate != null) {
            if (isItem) {
                return true;
            } else {
                title = description = null;
                return false;
            }
        }
        return false;
    }

    private void setContent(String name, String text) {
        if(isItem){
            if (nameIsTag(name, "title")) {
                title = text;
            } else if (nameIsTag(name, "description")) {
                description = text;
            } else if (nameIsTag(name, "pubDate")){
                pubDate = text;
            }
        }
    }

    private boolean typeIsTag(int eventType, int tagType) {
        return eventType == tagType;
    }

    private boolean nameIsTag(String name, String tag) {
        return name.equalsIgnoreCase(tag);
    }

    private String getImageUrl() {
        Document document = Jsoup.parse(description);
        Element img = document.select("[src]").first();
        return "http:" + img.attr("src");
    }

    private String getHandledDescription() {
        String string = Html.fromHtml(description).toString();
        return string.replace("￼\n\n", "");
    }

    private Date getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);
        try {
            return dateFormat.parse(pubDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
