package com.gym.personal.basicrssreader.Core.Contracts;

import java.io.InputStream;

public interface AsyncResponse {

    void onSuccess(InputStream result);
}
