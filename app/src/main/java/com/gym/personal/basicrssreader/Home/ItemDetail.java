package com.gym.personal.basicrssreader.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.gym.personal.basicrssreader.Core.Entities.Item;
import com.gym.personal.basicrssreader.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemDetail extends AppCompatActivity {

    @BindView(R.id.item_detail_title)
    TextView title;
    @BindView(R.id.item_detail_subtitle)
    TextView subtitle;
    @BindView(R.id.item_detail_image)
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        ButterKnife.bind(this);
        Intent i = getIntent();
        Item item = (Item) i.getSerializableExtra("item");

        title.setText(item.getTitle());
        subtitle.setText(item.getDescription());
        loadImage(image, item.getUrlImage());
    }

    private void loadImage(ImageView image, String urlImage) {
        Picasso.get().load(urlImage).into(image);
    }
}
