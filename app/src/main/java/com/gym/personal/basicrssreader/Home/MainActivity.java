package com.gym.personal.basicrssreader.Home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gym.personal.basicrssreader.Core.Entities.Item;
import com.gym.personal.basicrssreader.Home.Contracts.AsyncXmlResponse;
import com.gym.personal.basicrssreader.Home.Contracts.IHomeDomain;
import com.gym.personal.basicrssreader.R;
import com.gym.personal.basicrssreader.di.DaggerApplicationComponent;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private IHomeDomain homeDomain = DaggerApplicationComponent.create().getHomeDomain();

    @BindView(R.id.mainRecyclerView)
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setVisibility(View.VISIBLE);
        homeDomain.getItems(new AsyncXmlResponse() {
            @Override
            public void onSuccess(List<Item> items) {
                recycler.setAdapter(new HomeAdapter(items));
            }
        });
    }
}
