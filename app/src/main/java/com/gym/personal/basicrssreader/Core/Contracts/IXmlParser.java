package com.gym.personal.basicrssreader.Core.Contracts;

import com.gym.personal.basicrssreader.Core.Entities.Item;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface IXmlParser {

    List<Item> parseXml(InputStream xml) throws IOException;
}
