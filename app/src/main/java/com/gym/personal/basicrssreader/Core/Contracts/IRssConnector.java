package com.gym.personal.basicrssreader.Core.Contracts;

public interface IRssConnector {

    void getXmlFromUrl(String url, final AsyncResponse delegate);

}
