package com.gym.personal.basicrssreader.Core.Services;

import android.os.AsyncTask;

import com.gym.personal.basicrssreader.Core.Contracts.AsyncResponse;
import com.gym.personal.basicrssreader.Core.Contracts.IRssConnector;

import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RssConnector extends AsyncTask<String, String, InputStream> implements IRssConnector {

    private AsyncResponse delegate = null;

    @Override
    public void getXmlFromUrl(String url, final AsyncResponse delegate) {
        String[] params = {url};
        this.delegate = delegate;
        this.execute(params);
    }

    @Override
    protected InputStream doInBackground(String... strings) {
        String url = strings[0];
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            InputStream xml = response.body().byteStream();
            return xml;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(InputStream inputStream) {
        super.onPostExecute(inputStream);
        delegate.onSuccess(inputStream);
    }
}
