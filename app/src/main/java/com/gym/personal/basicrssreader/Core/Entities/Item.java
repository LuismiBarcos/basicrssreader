package com.gym.personal.basicrssreader.Core.Entities;

import java.io.Serializable;
import java.util.Date;

public class Item implements Serializable{
    private String title;
    private String urlImage;
    private String description;
    private Date pubDate;

    public Item(String title, String urlImage, String description, Date pubDate) {
        this.title = title;
        this.urlImage = urlImage;
        this.description = description;
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getDescription() {
        return description;
    }

    public Date getPubDate() {
        return pubDate;
    }

}
