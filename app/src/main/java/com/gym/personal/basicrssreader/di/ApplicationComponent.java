package com.gym.personal.basicrssreader.di;

import com.gym.personal.basicrssreader.Core.Contracts.IRssConnector;
import com.gym.personal.basicrssreader.Core.Contracts.IXmlParser;
import com.gym.personal.basicrssreader.Home.Contracts.IHomeDomain;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    IRssConnector getRssConnector();

    IXmlParser getXmlParser();

    IHomeDomain getHomeDomain();

}
